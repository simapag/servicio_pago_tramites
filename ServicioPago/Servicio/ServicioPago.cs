﻿using ServicioPago.Datos;
using ServicioPago.Modelo;
using ServicioPago.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using System.Timers;

namespace ServicioPago.Servicio
{
    public class Servicio_Tramistes_Pendientes
    {
        private Timer _timer;
        Cls_Correo cls_Correo = new Cls_Correo();

        public Servicio_Tramistes_Pendientes() {
            // 900000 = 15 minutos     // 600000 = 10 minutos  //  1200000 = 20 minutos // 3600000 = 60 minutos // 56250
            _timer = new Timer(7200000)
            { AutoReset = true };
            _timer.Elapsed += TimerElapsed;
        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            DateTime dateTimeHora = DateTime.Now;


            if (dateTimeHora.Hour >= 7 && dateTimeHora.Hour <= 22)
            {
                _timer.Enabled = false;
                Ejecutar_Proceso();
             
            }


        }

        public void Start()
        {
            _timer.Start();
        }


        public void Stop()
        {
            _timer.Stop();
        }


        public void Ejecutar_Proceso()
        {
            DataTable DatPagosPendientes;
            Cls_Pago_Datos cls_Pago_Datos = new Cls_Pago_Datos();
            int contador;

            SqlConnection ObjConexion = null;
            SqlTransaction ObjTransaccion = null;

            try
            {
                Cls_Pago cls_Pago;

                DatPagosPendientes = cls_Pago_Datos.Obtener_Pagos_Pendiente();

                if (DatPagosPendientes.Rows.Count > 0)
                {

                    for (contador = 0; contador < DatPagosPendientes.Rows.Count; contador++)
                    {
                        ObjConexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                        ObjConexion.Open();

                        cls_Pago = new Cls_Pago();
                        cls_Pago.Estatus = DatPagosPendientes.Rows[contador]["Estatus"].ToString();
                        cls_Pago.Tipo_Pago = DatPagosPendientes.Rows[contador]["Tipo_Pago"].ToString();
                        cls_Pago.Tipo_Tarjeta = DatPagosPendientes.Rows[contador]["Tipo_Tarjeta"].ToString();
                        cls_Pago.Referencia_Bancaria = DatPagosPendientes.Rows[contador]["Referencia_Bancaria"].ToString();
                        cls_Pago.No_Factura = DatPagosPendientes.Rows[contador]["No_Factura"].ToString();
                        cls_Pago.RPU = DatPagosPendientes.Rows[contador]["RPU"].ToString();
                        cls_Pago.Importe = Convert.ToDecimal(DatPagosPendientes.Rows[contador]["Importe"]);
                        cls_Pago.Folio_Pago = DatPagosPendientes.Rows[contador]["Folio_Pago"].ToString();
                        cls_Pago.Pagos_No_Aplicados_ID = Convert.ToInt32(DatPagosPendientes.Rows[contador]["Pagos_No_Aplicados_ID"]);

                        ObjTransaccion = ObjConexion.BeginTransaction();

                        cls_Pago_Datos.Pagar_Tramite(ObjConexion, ObjTransaccion, cls_Pago);
                        cls_Pago_Datos.Cambiar_Estatus(ObjConexion, ObjTransaccion, cls_Pago);

                        ObjTransaccion.Commit();

                    }

                    ObjConexion.Close();
                    ObjConexion = null;
                }

            }
            catch (Exception ex)
            {
                Utility.Logger(ex.Message);
                cls_Correo.Enviar_Correo_Error(ex.Message);

                if (ObjTransaccion != null)
                {
                    ObjTransaccion.Rollback();
                }

            }
            finally {
                _timer.Enabled = true;

                if (ObjConexion != null)
                {
                    ObjConexion.Close();
                    ObjConexion = null;
                }
            }
        }


    }
}
