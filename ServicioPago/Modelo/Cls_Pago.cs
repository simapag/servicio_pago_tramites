﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicioPago.Modelo
{
   public class Cls_Pago
    {

        public string Folio_Pago { get; set; }
        public string RPU { get; set; }
        public string No_Factura { get; set; }
        public string Referencia_Bancaria { get; set; }
        public string Tipo_Pago { get; set; }
        public string Tipo_Tarjeta { get; set; }
        public decimal Importe { get; set; }
        public string Estatus { get; set; }
        public int Pagos_No_Aplicados_ID { get; set; }
    }
}
