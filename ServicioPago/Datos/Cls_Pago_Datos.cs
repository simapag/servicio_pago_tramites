﻿using ServicioPago.Modelo;
using ServicioPago.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicioPago.Datos
{
   public class Cls_Pago_Datos
    {
        public string str_sql;
        public DataTable Obtener_Pagos_Pendiente()
        {
            DataTable dt = new DataTable();
            SqlDataReader dr;
            using (SqlConnection conexion = new SqlConnection(Cls_Constantes.Str_Conexion))
            {
                conexion.Open();

                using (SqlCommand comando = conexion.CreateCommand())
                {
                    str_sql = "sp_obtener_pagos_pendientes";
                    comando.CommandText = str_sql;
                    comando.CommandType = CommandType.StoredProcedure;
                    dr = comando.ExecuteReader();

                    dt.Load(dr);
                }
            }

            return dt;

        }

        public void Pagar_Tramite(SqlConnection conexion, SqlTransaction transaccion, Cls_Pago cls_Pago) {
            using (SqlCommand comando = conexion.CreateCommand())
            {

                comando.Transaction = transaccion;
                comando.CommandText = "ServidorSimapag.SIMAPAG.dbo.Sp_Tra_Pagar_Facturacion";
                comando.CommandType = CommandType.StoredProcedure;

                comando.Parameters.Add(new SqlParameter("@No_Factura", cls_Pago.No_Factura));
                comando.Parameters.Add(new SqlParameter("@Rpu", cls_Pago.RPU));
                comando.Parameters.Add(new SqlParameter("@Folio_Pago", cls_Pago.Folio_Pago));
                comando.Parameters.Add(new SqlParameter("@Referencia", cls_Pago.Referencia_Bancaria));
                comando.Parameters.Add(new SqlParameter("@Tipo_Pago", cls_Pago.Tipo_Pago));
                comando.Parameters.Add(new SqlParameter("@Tipo_Tarjeta", cls_Pago.Tipo_Tarjeta));
                comando.Parameters.Add(new SqlParameter("@Monto", cls_Pago.Importe));

                comando.ExecuteNonQuery();
            }
        }


        public void Cambiar_Estatus(SqlConnection conexion, SqlTransaction transaccion, Cls_Pago cls_Pago)
        {
            using (SqlCommand comando = conexion.CreateCommand())
            {

                comando.Transaction = transaccion;
                comando.CommandText = "sp_actualizar_estatus_pagado";
                comando.CommandType = CommandType.StoredProcedure;

                comando.Parameters.Add(new SqlParameter("@Pagos_No_Aplicados_ID", cls_Pago.Pagos_No_Aplicados_ID));
        

                comando.ExecuteNonQuery();
            }
        }
    }
}
