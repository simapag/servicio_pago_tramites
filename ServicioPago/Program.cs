﻿using ServicioPago.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace ServicioPago
{
    class Program
    {
        static void Main(string[] args)
        {
            var exitCode = HostFactory.Run(x =>
            {

                x.Service<Servicio_Tramistes_Pendientes>(s =>
                {
                    s.ConstructUsing(h => new Servicio_Tramistes_Pendientes());
                    s.WhenStarted(h => h.Start());
                    s.WhenStopped(h => h.Stop());

                });

                x.RunAsLocalSystem();

                x.SetServiceName("TramitesPendientesServicio");
                x.SetDisplayName("Servicio Que busca que pagos no aplicaron******************************************************");
                x.SetDescription("De los pagos que se hacen tramite busca que pagos no se completaron y los pagos");
            });


            int exitCodeValue = (int)Convert.ChangeType(exitCode, exitCode.GetTypeCode());
            Environment.ExitCode = exitCodeValue;
        }
    }
}
